#!/usr/bin/python

from docopt import docopt
import subprocess
import os
import sys

# EXPORT_PATH = "./builds"
EXPORT_PATH = "../distrib/builds"
PROJECT_PATH = "../game"
game_name = "geometry-cheat"

presets = {
        "html": {
            'ext': ".html"
            },
        "linux": {
            'ext': ".x86_64"
            },
        "windows": {
            'ext': ".exe"
            },
        }

def export_preset(preset_name):
    print("=== " + preset_name + " ===")

    if preset_name == "html":
        executable_name = "index"
    else:
        executable_name = game_name
    executable_name += presets[preset_name]['ext']

    build_path = EXPORT_PATH + "/" + build_name

    export_dir = build_path + "/" + preset_name
    export_fullpath = export_dir + "/" + executable_name


    if archive_build_name:
        archive_name = game_name + "-" + archive_build_name + "-" + preset_name
    else:
        archive_name = game_name + "-" + build_name + "-" + preset_name
    archive_path = build_path + "/" + archive_name
    archive_path_zip = archive_path + ".zip"

    if not archive_mode:
        if os.path.isdir(export_dir):
            print("Build directory already exists (" + export_dir + ") - SKIPPING")
            return
        print("exporting to " + export_fullpath)
        subprocess.run(["mkdir", "-p", export_dir])
        subprocess.run(["godot", "--path", PROJECT_PATH, "--export", preset_name, export_fullpath])

    if archive_mode or also_archive:
        if os.path.islink(archive_path):
            print("Build archive symlink already exists (" + archive_path + ") - SKIPPING")
            return
        if os.path.isfile(archive_path_zip):
            print("Build archive zipfile already exists (" + archive_path_zip + ") - SKIPPING")
            return
        print("archiving as " + archive_path)
        subprocess.run(["ln", "-sr", export_dir, archive_path])
        subprocess.run(["zip", "-r", archive_name + ".zip", archive_name], cwd=build_path)
        subprocess.run(["rm", archive_path])


doc = """Export helper for godot

Usage:
    export.py <preset_name> [-a] [--alt=<name>]
    export.py --archive <build_name> [--alt=<name>]
    export.py (-h | --help)
    export.py --version

Options:
    -a              Also archive the build
    --alt=<name>           Alternative build name to use for the archive [default: ]

"""

archive_mode = False
also_archive = False
archive_build_name = ""
build_name = 'build'

if __name__ == '__main__':
    args = docopt(doc, version='Export 0.1')
    print(args)
    print("--------")

    preset_name = args['<preset_name>']
    also_archive = args['-a']
    archive_mode = args['--archive']
    archive_build_name = args['--alt']

    if build_name == 'build':
        process = subprocess.run(["git", "describe"], capture_output=True)
        git_tag = process.stdout.rstrip().decode()
        if git_tag:
            build_name = git_tag
            print("git tag: " + build_name)

    print("--------\n")

    if archive_mode:
        build_name = args['<build_name>']
        preset_name = "all"

    if preset_name == "all":
        for p in presets.keys():
            export_preset(p)
    else:
        export_preset(preset_name)

