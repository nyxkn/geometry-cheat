extends Level

func _ready():
	dialogues = [
		["Welcome to the simulation, young warrior. You can continue and close these dialogues with SPACEBAR.",
		"[If you already know how to play, you can skip this tutorial level by pressing ESCAPE after the dialogue]"],
	#	["It's not a normal simulation. It's a COSMIC simulation. It's an allegory of the creation of the cosmos. Obviously...",
	#	"... hey I'm serious, I mean look at all the minimalistic art... this is not just a cheap attempt to fit the theme somehow."],
		"You can also jump with the SPACEBAR! I know, lots of different keys, make sure you don't forget them.",
		"By the way, you'll die if you hit those cubes and don't jump in time. But I'm sure you managed to avoid the first one.",
		"You have, haven't you? ... Anyway, let's try that again. Now a little higher!",
	#	Aha, learning quickly I see. Let's try that again.",
		"Beautiful. Where did you learn to flip in the air like that?",
		"Okay those jumps were EASY. But what about the next one? :) :) :)",
		"Hey, how did you get through that? Are you cheating? ... Just kidding, I made that wall clip-through for funsies.",
		["Okay but for real now, the same wall is coming up again. But this time you'll REALLY have to cheat.",
		"You'll focus your powers and summon a cheat code, all the while you'll be slowing down time (yes, you can do that).",
		"When the time comes, just TYPE the code keystrokes shown and you'll be granted a way to pass through the wall.",
		],
		["Ok, to be fair, this was clip-through too. There will be a few more of these so you can practice the timing.",
		"That back there was the SUPERJUMP. You'll also come across the DRILL superpower.",
		"It will be one of the two randomly. With the drill you can easily bash through the wall.",
		"But be careful, you DON'T want to jump while wielding the DRILL and end up stuck in the floor FOREVER."],
	]

