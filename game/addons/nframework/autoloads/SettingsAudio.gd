extends Node


var volume_master: float = 0.0 setget set_volume_master

onready var volume_master_bus: int = AudioServer.get_bus_index("Master")


func _ready():
	get_volumes()


func get_volumes() -> void:
	var volume_db = AudioServer.get_bus_volume_db(volume_master_bus)
	var volume_norm = range_lerp(volume_db, -80.0, 0, 0.0, 1.0)
	set_volume_master(volume_norm)


func set_volume_master(volume: float) -> void:
	volume_master = clamp(volume, 0.0, 1.0)
	var volume_db = lerp(-80, 0, volume_master)
	AudioServer.set_bus_volume_db(volume_master_bus, volume_db)
