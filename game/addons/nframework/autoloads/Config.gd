extends Node
# this file keeps all configuration. all of these things could be saved to a .cfg file for reloading

################################################################
# LOGGING

# Global switch for debug logs
var SHOW_LOG = true

# All LogCategories are shown by default. Add true to this Dictionary to
# prevent showing  Logs of this LogCategory
var HIDE_LOG_CATEGORY = {}

# All LogLevels are shown by default. Add true to this Dictionary to
# prevent showing Logs of this LogLevel
var HIDE_LOG_LEVEL = {}

################################################################
# SCREENS

# you can optionally override these if you need a different scene
var MAIN_MENU: String = "res://addons/nframework/screens/MainMenu.tscn"
var OPTIONS_MENU: String = "res://addons/nframework/screens/OptionsMenu.tscn"

# you must override this with your game actual entry point
var NEW_GAME: String = MAIN_MENU

################################################################
# OTHER
var LEVELS_PATH: String = "res://levels/"


################################################################
# GAME SETTINGS
# these could possibly go in a different "Settings" singleton?



################################################################
# INPUT

# https://docs.godotengine.org/en/stable/classes/class_%40globalscope.html#enum-globalscope-keylist
var INPUT_MAP = {
	"left": [KEY_A],
	"down": [KEY_S],
	"up": [KEY_W],
	"right": [KEY_D],
	"attack": [KEY_J],
	"interact": [KEY_K],
	}

var actions := {}

################################################################
# LOGIC

func replaceAction(action, event) -> void:
	InputMap.action_erase_event(action, actions[action])
	InputMap.action_add_event(action, event)
	actions[action] = event

func loadConfig() -> void:
	# ideally load from save/config file
	# InputMap is global so it persists everywhere
	for action in INPUT_MAP:
		InputMap.add_action(action)
		actions[action] = []
		for key in INPUT_MAP[action]:
			var event = InputEventKey.new()
			event.scancode = key
			InputMap.action_add_event(action, event)
			actions[action].append(event)
#            if actions.has(action):
#                actions[action].append(event)
#            else:
#                actions[action] = [event]

	printInputMap()
	printActions()

func printInputMap() -> void:   
	for a in InputMap.get_actions():
		# exclude/choose ui_ default mappings
		if a.left(3) == 'ui_':
			var log_str = str(a, ": ")
			for i in InputMap.get_action_list(a):
				log_str += str(i.as_text(), " ")
			Log.d("INPUT_DEFAULT", log_str)
	
func printActions() -> void:
	for action in actions.keys():
		var log_str = str(action, ": ")
		for key in actions[action]:
			log_str += str(key.as_text(), " ")
		Log.d("INPUT", log_str)

