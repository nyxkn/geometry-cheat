extends CanvasLayer
# game levels are responsible for setting (and unsetting!) can_show if they want to enable pausing

var can_show := false

onready var btn_Resume := find_node("Resume")

func _ready():
	set_volume_sliders()


func _unhandled_input(event) -> void:
	if event.is_action_pressed("ui_cancel"):
		if Game.paused:
			pause(false)
		elif can_show:
			pause(true)


func pause(paused: bool) -> void:
	Game.pause(paused)
	$Control.visible = paused
	if paused:
		btn_Resume.grab_focus()


func _on_Resume_pressed():
	pause(false)


################################################################
# Options stuff - this should belong somewhere in options/settings, not here

func set_volume_sliders():
#	print("setting pause slider to ", str(SettingsAudio.volume_master))
	find_node("HSlider").value = SettingsAudio.volume_master * 100


# ideally this logic should be in the options menu. but for now yagni
func _on_Volume_value_changed(value):
	SettingsAudio.volume_master = value / 100
