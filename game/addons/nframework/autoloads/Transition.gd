extends CanvasLayer


signal fade_out_completed
signal fade_in_completed

export var DURATION := 0.5
export var FADE_IN_ALPHA := 0.0
export var FADE_OUT_ALPHA := 1.0

const TRANS := Tween.TRANS_LINEAR
const EASE := Tween.EASE_IN

var fade_percent: float = 0 setget set_fade_percent

onready var colorRect := $ColorRect
onready var tween_out := $TweenOut
onready var tween_in := $TweenIn


func _ready() -> void:
	colorRect.modulate.a = 0
	colorRect.color.a = 1
	
	
func fade_out() -> void:
	tween_out.interpolate_property(self, "fade_percent", FADE_IN_ALPHA, FADE_OUT_ALPHA, DURATION, TRANS, EASE)
	tween_out.start()

	
func fade_in() -> void:
	tween_in.interpolate_property(self, "fade_percent", FADE_OUT_ALPHA, FADE_IN_ALPHA, DURATION, TRANS, EASE)
	tween_in.start()
	

func set_fade_percent(value: float) -> void:
	fade_percent = clamp(value, 0.0, 1.0)
	#Fade logic
	colorRect.modulate.a = fade_percent
	
	
func on_faded_out() -> void:
	emit_signal("fade_out_completed")
	
	
func on_faded_in() -> void:
	emit_signal("fade_in_completed")
	

func _on_TweenOut_tween_all_completed():
	on_faded_out()


func _on_TweenIn_tween_all_completed():
	on_faded_in()
