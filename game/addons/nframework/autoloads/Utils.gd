extends Node

# Utility functions we find ourselves rewriting often

func array_to_printable_string(array):
	var string := ""
	for e in array:
		string += str(e, ", ")
	return string.substr(0, string.length() - 2)

# random simple benchmark function. call this for 10-20 times to simulate a couple seconds lock
func benchmark_function():
	var lst = []
	for i in 999999:
		lst.append(sqrt(i))
