extends MarginContainer


func _ready():
	if OS.get_name() == "HTML5":
		$VBoxContainer/MenuOptions/Exit.visible = false


func _on_NewGame_pressed():
	Game.change_scene(Config.NEW_GAME)

func _on_Options_pressed():
	Game.change_scene(Config.OPTIONS_MENU)

#func _on_Credits_pressed():
#    pass # Replace with function body.

func _on_Exit_pressed():
	get_tree().quit()
