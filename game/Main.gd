extends Node

func _ready():
	# entry point for the whole application

	Config.HIDE_LOG_LEVEL = {
		Log.LogLevel.DEBUG: true,
		Log.LogLevel.INFO: false,
	}

	Config.loadConfig()
	Log.i("MAIN", "Config loaded")

	Config.NEW_GAME = "res://Game.tscn"

#	Framework.change_scene(Config.MAIN_MENU, false)
#	Framework.change_scene(Config.NEW_GAME, false)
	Framework.change_scene("res://screens/Intro.tscn", false)
