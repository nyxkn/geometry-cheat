extends KinematicBody2D

signal crashed

#export (int) var speed = 200

var scroll_speed = 400
var jump_speed = 1300
var superjump_speed = 2200
const GRAVITY_SPEED = 6000
var gravity: int

var velocity = Vector2()

var jumping
var alive
var digging
var superjump

var moving := false

func _ready():
	reset()

func reset():
	jumping = false
	alive = true
	moving = false
	gravity = GRAVITY_SPEED
	collision_mask = 0b111
	disable_dig()
	disable_superjump()
	$Sprite.rotation_degrees = 0

func jump_time(speed):
	var jump_time = 2.0 * speed / gravity
	return jump_time

func _input(event):
	if event.is_action_pressed("ui_select") and moving:
		if !jumping:
			jumping = true
			
			if superjump:
				velocity.y -= superjump_speed
				disable_superjump()
			else:
				velocity.y -= jump_speed

#			var tween = get_node("Tween")
			$Tween.interpolate_property($Sprite, "rotation_degrees",
#					rotation_degrees, rotation_degrees + 90, jump_time(abs(velocity.y)),
					0, 90 if velocity.y == -jump_speed else 270, jump_time(abs(velocity.y)),
					Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			$Tween.start()


#func _on_Tween_tween_all_completed():
#	if alive:
#		$Sprite.rotation_degrees = 0

func _on_Tween_tween_completed(object, key):
	if alive:
		$Sprite.rotation_degrees = 0
	
	
func _physics_process(delta):
	if alive:
		velocity.x = scroll_speed	
		velocity.y += gravity * delta
		if moving:
			velocity = move_and_slide(velocity, Vector2.UP)

		if jumping:
			if is_on_floor():
				jumping = false
				if digging:
					emit_signal("crashed")
					alive = false
					moving = false
					$Sprite/Drill.playing = false
					$AnimationPlayer.stop()
		
		if is_on_wall():
#			print("am_on_wall")
			emit_signal("crashed")
			alive = false
			moving = false
			$Tween.stop_all()
	
func enable_dig():
	digging = true
# layer 1 is floor layer 2 walls: 01 floor, 11 both
	collision_mask = 0b001
	$Sprite/Drill.show()
	$Sprite/DrillGlow.show()
	$AnimationPlayer.play("drill_glow")
	$Sprite/Drill.playing = true
#	yield(get_tree().create_timer(2.0), "timeout")
	
func disable_dig():
	if alive:
		$AnimationPlayer.stop()
		$Sprite/DrillGlow.hide()
		$Sprite/Drill.hide()
		digging = false
		
		yield(get_tree().create_timer(0.1), "timeout")
		collision_mask = 0b011

func enable_superjump():
	superjump = true
	$Sprite/Glow.show()
	$AnimationPlayer.play("glow")
#	yield(get_tree().create_timer(2.0), "timeout")


func disable_superjump():
	superjump = false
	$AnimationPlayer.stop()
	$Sprite/Glow.hide()




