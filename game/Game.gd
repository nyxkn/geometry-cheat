extends Node

onready var TextBox := $TextBox
onready var UI := $UILayer/UI
onready var TimerLabel := $UILayer/UI/TimerLabel

onready var CheatLabels := $UILayer/UI/CheatLabels
onready var CheatBackground := $UILayer/UI/CheatLabels/CenterContainer/CheatBackground
onready var CheatProgress := $UILayer/UI/CheatLabels/CenterContainer/CheatProgress
onready var CheatEffectLabel := $UILayer/UI/CheatLabels/CheatEffect

onready var ll := $LevelLoader

#const PATH_LEVELS = "res://levels/"
#var levels_filenames = []
#var current_level_id
#var current_level: Node

var tutorial_first_jump := true

var cheatcode := ""
var cheatcode_idx := 0
var cheat_type
var cheat_effect
#var typing := false
var cheat_outcome := 0
var cheat_ready := true

var cheating := false
var slowmo_start_time := 0.0
var slowmo_end_time := 0.0
var slowmo_factor := 0.0
var slowmo_duration := 0.0

var about_to_start_cheating := false

var wall_entered := false

var gameover := false
var changing_level := true
#var intro := true

var actually_cheating := false

#var scroll_speed := 250

enum CheatType { PC, CONSOLE }
enum CheatEffect { DRILL, SUPERJUMP }

# pc: doom, aoe, sims
# console: gta, konami, mortal kombat
# cheats: money, invincibility

# maybe best to randomly generate codes?

#var cheats_pc := [
#	"rosebud",
#	"motherlode",
#	"helpusobi 1",
#	"justin bailey",
#	"how do you turn this on",
#	"iddqd",
#]

var cheats_pc := [
	"iddqd",
	"idclip",
	"idkfa",
	"idchoppers",
	"idbeholds",
	"cockadoodledoo",
	"giveammo",
	"giveweapons",
	"giveall",
	"givehealth",
	"impulse9",
	"notarget",
	"noclip",
	"givebfg10k",
	"givebfg",
	"god",
	"playbetter",
	"twoweeks",
	"satan",
	"clubmed",
	"butcher",
	"nra",
	"indiana",
	"deliverance",
	"conan",
	"martek",
	
	"movetoqt",
	"advskill",
	"tgm",
	
	"whosyourdaddy",
	"iseedeadpeople",
	"thereisnospoon",
	"greedisgood",
	
	"showmethemoney",
	"medievalman",
	"operationcwal",
	"gameoverman",
	"foodforthought",
	"blacksheepwall",
	
	
	"pepperonipizza",
	"coinage",
	"quarry",
	"woodstock",
	"diediedie",
	"bigdaddy",
	"bigmomma",
	"hoyohoyo",
	"photonman",
	"emc2trooper",
#	"howdoyouturnthison",
	
	"klapaucius",
	"rosebud",
	"motherlode",
	
	"nowisee",
]

var cheats_console := [
	"abacabb",
	"uuddlrlrba",
]

var dialogue_ready = true
var dialogue_idx = 0


func _ready():
	randomize()
	
#	Hud.visible = true
	Pause.can_show = false
	
	$Player.connect("crashed", self, "_on_Player_crashed")
	
	$TextBox.connect("textbox_reading", self, "_on_TextBox_reading")
	$TextBox.connect("textbox_cleared", self, "_on_TextBox_cleared")

	$LevelLoader.connect("level_faded_out", self, "on_level_faded_out")
	$LevelLoader.connect("level_loaded", self, "on_level_loaded")
	$LevelLoader.connect("level_changed", self, "on_level_changed")
	$LevelLoader.connect("level_faded_in", self, "on_level_faded_in")

	$LevelLoader.init_levels()
	$LevelLoader.change_level(0, false)

func _on_TextBox_reading():
	get_tree().paused = true
	
func _on_TextBox_cleared():
	get_tree().paused = false
	
func _exit_tree() -> void:
#	Hud.visible = false
	Pause.can_show = false

func get_tile_name_from_world_pos(tilemap, position):
	var tile_pos = tilemap.world_to_map(position)
	var tile_id = tilemap.get_cellv(tile_pos)
	if tile_id >= 0:
		var tile_name = tilemap.tile_set.tile_get_name(tile_id)	
		return tile_name
	else:
		return ""

func _process(delta) -> void:
	if changing_level: return
	
	var current_level = ll.current_level
	
#	$Player.velocity.x = scroll_speed
	$Camera2D.position.x = $Player.position.x + 240

	var current_trigger_tile = get_tile_name_from_world_pos(current_level.get_node("Triggers"), $Player.position)
	
	if cheating:
		var remaining_time = slowmo_duration * 1000 - (OS.get_ticks_msec() - slowmo_start_time)
		TimerLabel.text = msec_to_clock(remaining_time)
	
	if !cheating and cheat_ready and !about_to_start_cheating:
		if current_trigger_tile == "trigger":
			about_to_start_cheating = true
			start_cheat()
					
	if current_trigger_tile == "dialogue" and dialogue_ready:
		if dialogue_idx < current_level.dialogues.size():
			var dialogue = current_level.dialogues[dialogue_idx]
			if dialogue is Array:
				$TextBox.set_text(dialogue)
			else:
				$TextBox.queue_text(dialogue)
			dialogue_idx += 1
			dialogue_ready = false
			$DialogueCooldown.start()
	
	if current_trigger_tile == "level":
		pass
		next_level()
	
	
	
	if $Player.digging:
		var tile_name = get_tile_name_from_world_pos(current_level.get_node("Walls"), $Player.position)
		if tile_name == "wall" or tile_name == "wall-fake":
			wall_entered = true
		else:
			if wall_entered:
				$Player.disable_dig()
				wall_entered = false


func start_cheat():
	yield(get_tree().create_timer(0.2), "timeout")
	cheating = true
	slowmo_start_time = OS.get_ticks_msec()
	slowmo_duration = 4
	if ll.current_level_id == 0: slowmo_duration = 8
	Engine.time_scale = 0.1

	$SlowmoTimer.wait_time = float(slowmo_duration * Engine.time_scale)
	$SlowmoTimer.start()
	TimerLabel.show()
	$Player.set_process_input(false)

	if randf() < 0.5:
		cheat_effect = CheatEffect.SUPERJUMP
	else:
		cheat_effect = CheatEffect.DRILL
	
	if actually_cheating:
		cheat_effect = CheatEffect.DRILL

	if ll.current_level_id == 0 and tutorial_first_jump:
		cheat_effect = CheatEffect.SUPERJUMP
		tutorial_first_jump = false
	
	cheats_pc.shuffle()
	cheatcode = cheats_pc[0].to_upper()
	cheat_type = CheatType.PC
	cheatcode_idx = 0
	cheat_outcome = 0
	
	CheatBackground.text = cheatcode
	CheatProgress.text = cheatcode
	CheatProgress.visible_characters = 0
	CheatEffectLabel.text = CheatEffect.keys()[cheat_effect]
#	CheatEffectLabel.get_node("FlashText").start()
	$FlashText.wait_time = 0.3 * Engine.time_scale
	$FlashText.start()
	
	CheatLabels.show()
	CheatLabels.get_node("CheatEffect").show()
	
	cheat_ready = false
	$CheatCooldown.start()

	if actually_cheating:
		end_cheat()


func _on_FlashText_timeout():
	CheatEffectLabel.visible = !CheatEffectLabel.visible
	
#	cheatcode = "uuddlrlrba".to_upper()
#	cheat_type = CheatType.CONSOLE
#	cheatcode_idx = 0

func end_cheat():
	$SlowmoTimer.stop()
	$FlashText.stop()
	Engine.time_scale = 1
	cheating = false
	about_to_start_cheating = false
	TimerLabel.hide()
	CheatLabels.hide()
#	typing = false
	$Player.set_process_input(true)
	
	if cheat_outcome == 1 or actually_cheating:
		match cheat_effect:
			CheatEffect.DRILL: $Player.enable_dig()
			CheatEffect.SUPERJUMP: $Player.enable_superjump()
#	else:
#		print("fail")

func _on_SlowmoTimer_timeout():
	end_cheat()

func _on_CheatCooldown_timeout():
	cheat_ready = true

func _on_DialogueCooldown_timeout():
	dialogue_ready = true
	
func _on_Player_crashed():
	if changing_level: return
		
	gameover = true
	$Player.set_process_input(false)
	$Player.moving = false
	
	
	$UILayer/Gameover.show()
	if $Player.digging:
		$UILayer/Gameover.death_reason("stuck")
	else:
		$UILayer/Gameover.death_reason("crashed")

	
func msec_to_clock(msec):
#	print(msec)
	var msec_str = str(msec).pad_zeros(4)
	var clock = "0" + msec_str[0] + ":" + msec_str.substr(1, 2)
	return clock

func letter_to_console(letter):
	var key = ""
	match letter:
		'L': key = "Left"
		'D': key = "Down"
		'U': key = "Up"
		'R': key = "Right"
		_: key = letter
	return key

func _input(event):
	if ll.current_level_id == 0 and event.is_action_pressed("ui_cancel"):
		$Player.moving = false
		next_level()
		
	if cheating:
		if event is InputEventKey and event.is_pressed() and !event.is_echo():
			var key = OS.get_scancode_string(event.scancode)
			var cheat_next_input = cheatcode[cheatcode_idx]
			if cheat_type == CheatType.CONSOLE:
				cheat_next_input = letter_to_console(cheat_next_input)
			if key == cheat_next_input:
#				print(key)
				cheatcode_idx += 1
				CheatProgress.visible_characters += 1
				if cheatcode_idx >= cheatcode.length():
					cheat_outcome = 1
					end_cheat()

	if gameover:
		if event.is_action_pressed("ui_select"):
			gameover = false
			$Player.moving = false
			$UILayer/Gameover.hide()
			restart_level()
			

func restart_level():
	changing_level = true
	$LevelLoader.change_level(ll.current_level_id)


func next_level():
	changing_level = true

	var next_level_id = ll.current_level_id + 1

	if next_level_id >= ll.level_files.size():
		if ! Framework.change_scene("res://screens/End.tscn", true):
			print("Something went terribly wrong...")
	else:
		ll.change_level(next_level_id, true, true)


#func change_level(level_id: int):
#	ll.change_level(level_id)

func on_level_faded_out():
	$UILayer/Gameover.reset()


func on_level_loaded():
	var walls = ll.next_level.get_node("Walls")
	if walls: walls.collision_layer = 0b000
	var floor_layer = ll.next_level.get_node("Floor")
	if floor_layer: floor_layer.collision_layer = 0b000
	
	ll.next_level.get_node("Triggers").hide()


func on_level_changed():
	dialogue_idx = 0	

	end_cheat()
	cheating = false
	
	var walls = ll.current_level.get_node("Walls")
	if walls: walls.collision_layer = 0b010
	var floor_layer = ll.current_level.get_node("Floor")
	if floor_layer: floor_layer.collision_layer = 0b001
	
	$Player.position = Vector2(240, 456)
	$Player.reset()
	changing_level = false


func on_level_faded_in():
#	yield(get_tree().create_timer(0.5), "timeout")
	$Player.moving = true
	$Player.set_process_input(true)
	
