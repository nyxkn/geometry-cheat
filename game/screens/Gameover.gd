extends Control

func _ready():
	pass

func _on_Gameover_visibility_changed():
	if visible:
		$Transition.fade_out()
		$CenterContainer/VBoxContainer/Stuck.hide()
		$CenterContainer/VBoxContainer/Crashed.hide()
#	else:
#		$Transition.fade_in()

func reset():
	$Transition.get_node("ColorRect").modulate.a = 0

func _on_FlashText_timeout():
	$Continue.visible = !$Continue.visible

func death_reason(reason):
	match reason:
		"stuck": $CenterContainer/VBoxContainer/Stuck.show()
		"crashed": $CenterContainer/VBoxContainer/Crashed.show()
